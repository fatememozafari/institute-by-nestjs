import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';

export function adminObj(admin, jwtToken = null) {
  return {
    id: admin.id,
    userName: admin.userName,
    jwtToken,
  };
}
export class AdminDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @ApiProperty({ type: String })
  jwtToken: string;
}
export class LoginAdminDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  userName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  password: string;
}

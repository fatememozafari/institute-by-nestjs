import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';

export function categoryObj(category) {
  return {
    id: category.id,
    name: category.name,
    createdAt: category.createdAt,
  };
}
export class CategoryDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;
}
export class AdminCreateCategoryDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;
}

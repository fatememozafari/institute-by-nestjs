import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import { UserDto, userObj } from './user.dto';
import { CategoryDto, categoryObj } from './category.dto';

export function contactObj(contact) {
  const Category = categoryObj(contact.Category);
  const User = userObj(contact.User);
  return {
    id: contact.id,
    name: contact.name,
    title: contact.title,
    message: contact.message,
    file: contact.file,
    answer: contact.answer,

    userId: contact.userId,
    User,

    categoryId: contact.categoryId,
    Category,

    createdAt: contact.createdAt,
  };
}
export class ContactDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  message: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  file: string;

  @ApiProperty({ type: String })
  answer: string;

  @ApiProperty({ type: Number })
  userId: number;

  @ApiProperty({ type: UserDto })
  User: UserDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;

  @ApiProperty({ type: CategoryDto })
  Category: CategoryDto;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  createdAt: Date;
}
export class CreateContactDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  message: string;

  @ApiProperty({ type: String })
  file: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;
}

export class AdminAnswerToContactDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  answer: string;
}

import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { UserDto, userObj } from './user.dto';
import { CategoryDto, categoryObj } from './category.dto';
import courseStatus from 'src/common/eNums/courseStatus.enum';
import courseTypes from '../common/eNums/courseTypes.enum';

export function courseObj(course) {
  const Category = categoryObj(course.Category);
  // const User = userObj(course.User);

  const status = Object.keys(courseStatus).find(
    (key) => courseStatus[key].code === course.status,
  );
  const courseType = Object.keys(courseTypes).find(
    (key) => courseTypes[key].code === course.courseType,
  );

  return {
    id: course.id,
    title: course.title,
    slug: course.slug,
    startDate: course.startDate,
    endDate: course.endDate,
    rate: course.rate,
    duration: course.duration,
    teacher: course.teacher,
    status: course.status,
    statusText: courseStatus[status].text,
    courseType: course.courseType,
    //courseTypeText: courseTypes[courseType].text,
    // userId: course.userId,
    // User,
    categoryId: course.categoryId,
    Category,
    createdAt: course.createdAt,
  };
}
export class CourseDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  courseType: number;

  //@ApiProperty({ type: String })
  //courseTypeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  startDate: Date;

  @ApiProperty({ type: Date })
  endDate: Date;

  @ApiProperty({ type: Number })
  duration: number;

  @ApiProperty({ type: String })
  teacher: string;

  @ApiProperty({ type: Number })
  rate: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  status: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  statusText: string;

  // @IsNotEmpty()
  // @ApiProperty({ type: Number })
  // userId: number;

  // @IsNotEmpty()
  // @ApiProperty({ type: UserDto })
  // User: UserDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;

  @IsNotEmpty()
  @ApiProperty({ type: CategoryDto })
  Category: CategoryDto;
}
export class AdminCreateCourseDto {
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  courseType: number;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  startDate: Date;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  endDate: Date;

  @ApiProperty({ type: Number })
  duration: number;

  //@ApiProperty({ type: Number })
  //status: number;

  //@ApiProperty({ type: Number })
  //rate: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  teacher: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;
}
export class AdminUpdateCourseDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  startDate: Date;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  endDate: Date;

  @ApiProperty({ type: Number })
  duration: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  status: number;
}
export class FilterCourseDto {
  @IsOptional()
  @ApiProperty({ type: String, required: false })
  title: string;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  courseType: number;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  categoryId: number;

  @IsOptional()
  @ApiProperty({ type: Date, required: false })
  startDate: Date;

  @IsOptional()
  @ApiProperty({ type: Date, required: false })
  endDate: Date;

  // @IsOptional()
  // @ApiProperty({ type: Number, required: false })
  // pageSize: number;

  // @IsOptional()
  // @ApiProperty({ type: Number, required: false })
  // page: number;
}

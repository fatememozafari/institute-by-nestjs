import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import { UserDto, userObj } from './user.dto';
import { CourseDto, courseObj } from './course.dto';

export function enrollObj(enroll) {
  const Course = courseObj(enroll.Course);
  const User = userObj(enroll.User);

  return {
    id: enroll.id,

    userId: enroll.userId,
    User,

    courseId: enroll.courseId,
    Course,
    score: enroll.score,
    createdAt: enroll.createdAt,
  };
}
export class EnrollDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: Number })
  userId: number;

  @ApiProperty({ type: UserDto })
  User: UserDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  courseId: number;

  @ApiProperty({ type: CourseDto })
  Course: CourseDto;

  @ApiProperty({ type: Number })
  score: number;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  createdAt: Date;
}
export class CreateEnrollDto {
  //@IsNotEmpty()
  //@ApiProperty({ type: Number })
  // userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  courseId: number;
}

export class ScoreToCourseDto {
  //  @IsNotEmpty()
  //@ApiProperty({ type: Number })
  //id: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  userId: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  courseId: number;

  @ApiProperty({ type: Number })
  score: number;
}

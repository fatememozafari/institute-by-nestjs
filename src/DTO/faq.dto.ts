import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import faqStatuses from 'src/common/eNums/faqStatuses.enum';
import { CategoryDto, categoryObj } from './category.dto';

export function faqObj(faq) {
  const Category = categoryObj(faq.Category);

  return {
    id: faq.id,
    question: faq.question,
    answer: faq.answer,
    file: faq.file,
    categoryId: faq.categoryId,
    Category,
    createdAt: faq.createdAt,
  };
}
export class FaqDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  question: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  answer: string;

  @ApiProperty({ type: String })
  file: string;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  createdAt: Date;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;

  @IsNotEmpty()
  @ApiProperty({ type: CategoryDto })
  Category: CategoryDto;
}
export class AdminCreateFaqDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  question: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  answer: string;

  @ApiProperty({ type: String })
  file: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;
}

import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsOptional } from 'class-validator';
import { UserDto, userObj } from './user.dto';
import { CategoryDto, categoryObj } from './category.dto';
import courseStatus from 'src/common/eNums/courseStatus.enum';
import courseTypes from 'src/common/eNums/courseTypes.enum';
import { adminObj } from './admin.dto';

export function offlineCourseObj(offlineCourse) {
  const Category = categoryObj(offlineCourse.Category);
  // const Admin = adminObj(offlineCourse.Admin);

  const status = Object.keys(courseStatus).find(
    (key) => courseStatus[key].code === offlineCourse.status,
  );

  const courseType = Object.keys(courseTypes).find(
    (key) => courseTypes[key].code === offlineCourse.courseType,
  );

  return {
    id: offlineCourse.id,
    title: offlineCourse.title,
    slug: offlineCourse.slug,
    description: offlineCourse.description,
    file: offlineCourse.file,
    rate: offlineCourse.rate,
    duration: offlineCourse.duration,
    teacher: offlineCourse.teacher,
    status: offlineCourse.status,
    statusText: courseStatus[status].text,
    courseType: offlineCourse.courseType,
    courseTypeText: courseTypes[courseType].text,
    // adminId: offlineCourse.adminId,
    // Admin,
    categoryId: offlineCourse.categoryId,
    Category,
    createdAt: offlineCourse.createdAt,
  };
}
export class OfflineCourseDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  courseType: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  courseTypeText: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  duration: number;

  @ApiProperty({ type: String })
  teacher: string;

  @ApiProperty({ type: Number })
  rate: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  status: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  statusText: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  file: string;

  @ApiProperty({ type: String })
  description: string;

  // @IsNotEmpty()
  // @ApiProperty({ type: Number })
  // adminId: number;

  // @IsNotEmpty()
  // @ApiProperty({ type: UserDto })
  // Admin: UserDto;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;

  @IsNotEmpty()
  @ApiProperty({ type: CategoryDto })
  Category: CategoryDto;
}
export class AdminCreateOfflineCourseDto {
  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  courseType: number;

  // @IsNotEmpty()
  // @ApiProperty({ type: Number })
  // status: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  file: string;

  @ApiProperty({ type: String })
  description: string;

  @ApiProperty({ type: Number })
  duration: number;

  // @ApiProperty({ type: Number })
  // rate: number;

  @ApiProperty({ type: String })
  teacher: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  categoryId: number;
}
export class AdminUpdateOfflineCourseDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: String })
  title: string;

  @ApiProperty({ type: String })
  slug: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  description: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  file: string;

  @ApiProperty({ type: Number })
  duration: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  status: number;
}
export class FilterOfflineCourseDto {
  @IsOptional()
  @ApiProperty({ type: String, required: false })
  title: string;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  courseType: number;

  @IsOptional()
  @ApiProperty({ type: Number, required: false })
  categoryId: number;
}

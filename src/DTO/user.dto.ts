import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import userGender from '../common/eNums/userGender.enum';
import userStatus from '../common/eNums/userStatus.enum';

export function userObj(user, jwtToken = null) {
  const gender = Object.keys(userGender).find(
    (key) => userGender[key].code === user.gender,
  );
  const status = Object.keys(userStatus).find(
    (key) => userStatus[key].code === user.status,
  );
  return {
    id: user.id,
    firstName: user.firstName,
    lastName: user.lastName,
    mobile: user.mobile,
    email: user.email,
    avatar: user.avatar,
    rate: user.rate,
    address: user.address,
    gender: user.gender,
    genderText: userGender[gender].text,
    status: user.status,
    statusText: userStatus[status].text,
    createdAt: user.createdAt,
    jwtToken,
  };
}
export class UserDto {
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  id: number;

  @ApiProperty({ type: String })
  firstName: string;

  @ApiProperty({ type: String })
  lastName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  email: string;

  @ApiProperty({ type: String })
  address: string;

  @ApiProperty({ type: String })
  avatar: string;

  @ApiProperty({ type: Number })
  rate: number;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  gender: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  genderText: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  status: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  statusText: string;

  @ApiProperty({ type: String })
  jwtToken: string;
}
export class CreateUserDto {
  @ApiProperty({ type: String })
  firstName: string;

  @ApiProperty({ type: String })
  lastName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  email: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  password: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  passwordConfirmation: string;

  @ApiProperty({ type: String })
  address: string;

  @ApiProperty({ type: String })
  avatar: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  gender: number;
}
export class UpdateUserDto {
  @ApiProperty({ type: String })
  firstName: string;

  @ApiProperty({ type: String })
  lastName: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  email: string;

  @ApiProperty({ type: String })
  password: string;

  @ApiProperty({ type: String })
  passwordConfirmation: string;

  @ApiProperty({ type: String })
  address: string;

  @ApiProperty({ type: String })
  avatar: string;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  gender: number;
}
export class LoginUserDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  mobile: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  password: string;
}
export class AdminUpdateUserStatusDto {
  @IsNotEmpty()
  @IsInt()
  @ApiProperty({ type: Number })
  id: number;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  status: string;
}

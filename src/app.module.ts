import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ThrottlerModule } from '@nestjs/throttler';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Jwt } from './common/helpers/jwt.helper';
import sequilzeObj from './database/sequilze.obj';
import { UserModule } from './application/user/user.module';
import { AdminModule } from './application/admin/admin.module';
import { CategoryModule } from './application/category/category.module';
import { FaqModule } from './application/faq/faq.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    sequilzeObj,
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
    UserModule,
    AdminModule,
    CategoryModule,
    FaqModule,
  ],
  controllers: [],
  providers: [Jwt],
})
export class AppModule {}

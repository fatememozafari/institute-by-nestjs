import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Req,
  Headers,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { AdminService } from './admin.service';

import { adminObj, AdminDto, LoginAdminDto } from '../../DTO/admin.dto';

@ApiTags('admins/login')
@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}
  // login ************************************
  @ApiOkResponse({
    description: 'user login successful',
    type: AdminDto,
  })
  @ApiOperation({ summary: 'Login registered admin' })
  @ApiNotFoundResponse({ description: 'admin not found' })
  @ApiInternalServerErrorResponse({
    description: 'admin faild',
  })
  @Post('')
  @HttpCode(HttpStatus.OK)
  async login(
    @Res() res: Response,
    @Body() loginAdminDto: LoginAdminDto,
  ): Promise<AdminDto> {
    try {
      if (res.locals && res.locals.admin) {
        return res.locals.admin;
      }
      const admin = await this.adminService.login(loginAdminDto);
      res.status(200).json(admin);
      return;
    } catch (err) {
      throw err;
    }
  }
  // logOut ********************************
  @ApiOperation({ summary: 'logOut admin' })
  @ApiOkResponse({
    description: 'logOut',
    type: Boolean,
  })
  @ApiNotFoundResponse({ description: 'admin not found' })
  @ApiInternalServerErrorResponse({
    description: 'logout faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('signOut')
  async signOut(
    @Headers() headers,
    @Res() res: Response,
    @Req() req: Request,
  ): Promise<boolean> {
    try {
      const { jtoken } = headers;
      await this.adminService.signOut(jtoken);

      req.session.destroy((e) => console.log(e));
      res.status(200).json(true);
      return;
    } catch (err) {
      throw err;
    }
  }
}

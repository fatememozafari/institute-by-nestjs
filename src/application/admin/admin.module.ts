import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helper';
import { ConvertDate } from '../../common/helpers/convertDate.helper';
import { ValidAdminMiddleware } from '../../common/middlewares/validateAdmin.middleware';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';
import { ContactModule } from './contactUs/contact.module';
import { OfflineCourseModule } from './offlineCourse/offlineCourse.module';
import { CourseModule } from './course/course.module';
import { FaqModule } from './faq/faq.module';
import { EnrollModule } from './enroll/enroll.module';
import { CategoryModule } from './category/category.module';

@Module({
  imports: [
    CategoryModule,
    ContactModule,
    OfflineCourseModule,
    CourseModule,
    FaqModule,
    EnrollModule,
  ],
  controllers: [AdminController],
  providers: [AdminService, AdminDataAccess, Jwt, Tools, ConvertDate],
})
export class AdminModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ValidAdminMiddleware)
      .forRoutes(
        'admin/contactUs',
        'admin/categories',
        'admin/courses',
        'admin/enrolls',
        'admin/faqs',
        'admin/offlineCourses',
        {
          path: 'admin',
          method: RequestMethod.PUT,
        },
      );
  }
}

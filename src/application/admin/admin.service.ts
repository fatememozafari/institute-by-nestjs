import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import md5 = require('md5');
import { AdminDto, adminObj, LoginAdminDto } from '../../DTO/admin.dto';
import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helper';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';

@Injectable()
export class AdminService {
  constructor(
    private readonly adminDataAcceess: AdminDataAccess,
    private readonly jwt: Jwt,
    private readonly tools: Tools,
  ) {}
  // login ***************************************************
  async login(loginAdminDto: LoginAdminDto): Promise<AdminDto> {
    const { userName, password } = loginAdminDto;
    const admin = await this.adminDataAcceess.findAdminByuserName(userName);
    if (!admin) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Login failed',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (admin.password !== md5(password)) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Login failed',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    const tokenValues = {
      adminId: admin.id,
      type: 'admin',
    };
    const jwtToken = this.jwt.signer(tokenValues, 86400 * 2);
    await this.adminDataAcceess.updataJwtToken(admin.id, jwtToken);
    return adminObj(admin, jwtToken);
  }
  // logOut ********************
  async signOut(jwtToken) {
    const admin = await this.adminDataAcceess.findAdminToken(jwtToken);
    if (admin) {
      await this.adminDataAcceess.signOut(admin.id);
    }
    return;
  }
}

import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { CategoryService } from './category.service';

import { AdminCreateCategoryDto, CategoryDto } from '../../../DTO/category.dto';

@ApiTags('admin/category')
@Controller('admin/category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}
  // create category ********************************
  @Post('')
  @ApiOperation({ summary: 'create new category ' })
  @ApiOkResponse({
    description: 'category info',
    type: CategoryDto,
  })
  @ApiNotFoundResponse({ description: 'create failed' })
  @ApiInternalServerErrorResponse({
    description: 'create faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async createNewDepartment(
    @Res() res: Response,
    @Body() createCategoryDto: AdminCreateCategoryDto,
  ): Promise<CategoryDto> {
    try {
      const category = await this.categoryService.adminCreateCategory(
        createCategoryDto,
      );
      res.status(200).json(category);
      return;
    } catch (err) {
      throw err;
    }
  }
}

import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { CategoryDataAccess } from '../../../dataAccess/category.dataAccess';
import { ValidAdminMiddleware } from 'src/common/middlewares/validateAdmin.middleware';
import { Jwt } from 'src/common/helpers/jwt.helper';
import { Tools } from 'src/common/helpers/tools.helper';
import { ConvertDate } from 'src/common/helpers/convertDate.helper';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';

@Module({
  imports: [],
  controllers: [CategoryController],
  providers: [
    CategoryService,
    CategoryDataAccess,
    AdminDataAccess,
    Jwt,
    Tools,
    ConvertDate,
  ],
})
export class CategoryModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes('admin/category');
  }
}

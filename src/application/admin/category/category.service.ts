import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import {
  AdminCreateCategoryDto,
  CategoryDto,
  categoryObj,
} from 'src/DTO/category.dto';
import { CategoryDataAccess } from '../../../dataAccess/category.dataAccess';

@Injectable()
export class CategoryService {
  constructor(private readonly categoryDataAccess: CategoryDataAccess) {}
  //create category*******************
  async adminCreateCategory(
    createCategoryDto: AdminCreateCategoryDto,
  ): Promise<CategoryDto> {
    const { name } = createCategoryDto;
    const category = await this.categoryDataAccess.adminCreateCategory(name);
    const newCategory = await this.categoryDataAccess.findCategoryById(
      category.id,
    );
    return categoryObj(newCategory);
  }
}

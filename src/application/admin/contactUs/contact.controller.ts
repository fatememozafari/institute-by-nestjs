import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Param,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AdminAnswerToContactDto, ContactDto } from 'src/DTO/contactUs.dto';
import { ContactService } from './contact.service';

@ApiTags('admin/contactUs')
@Controller('contactUs')
export class ContactController {
  constructor(private readonly contactService: ContactService) {}
  //***********************contact list*********************** */
  @ApiOperation({ summary: 'get contact list' })
  @ApiOkResponse({
    description: ' contact list',
    type: ContactDto,
  })
  @ApiNotFoundResponse({ description: 'contact not found' })
  @ApiInternalServerErrorResponse({
    description: ' get list faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('contactList')
  async contactList(@Res() res: Response): Promise<ContactDto> {
    try {
      const contact = await this.contactService.contactList();
      res.status(200).json(contact);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** show contact info ************************************
  @ApiOkResponse({
    description: 'find successfully',
    type: ContactDto,
  })
  @ApiOperation({ summary: 'show contact info' })
  @ApiNotFoundResponse({ description: 'contact not found' })
  @ApiInternalServerErrorResponse({
    description: 'contact faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @Get('detail/:contactId')
  @HttpCode(HttpStatus.OK)
  async showContactInfo(
    @Res() res: Response,
    @Param('contactId') contactId: number,
  ): Promise<ContactDto> {
    try {
      const contact = await this.contactService.showContactInfo(contactId);
      res.status(200).json(contact);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** answer to contact ********************************
  @ApiOperation({ summary: 'update contact' })
  @ApiOkResponse({
    description: 'update contact',
    type: ContactDto,
  })
  @ApiNotFoundResponse({ description: 'contact not found' })
  @ApiInternalServerErrorResponse({
    description: 'contact faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Put('')
  async answerToContact(
    @Res() res: Response,
    @Body() answerToContact: AdminAnswerToContactDto,
  ): Promise<ContactDto> {
    try {
      const contact = await this.contactService.answerToMessage(
        answerToContact,
      );
      res.status(200).json(contact);
      return;
    } catch (err) {
      throw err;
    }
  }
}

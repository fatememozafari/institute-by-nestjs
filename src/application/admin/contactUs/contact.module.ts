import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { ConvertDate } from 'src/common/helpers/convertDate.helper';
import { Jwt } from 'src/common/helpers/jwt.helper';
import { Tools } from 'src/common/helpers/tools.helper';
import { ValidAdminMiddleware } from 'src/common/middlewares/validateAdmin.middleware';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { ContactDataAccess } from 'src/dataAccess/contactUs.dataAccess';
import { ContactController } from './contact.controller';
import { ContactService } from './contact.service';

@Module({
  imports: [],
  controllers: [ContactController],
  providers: [
    AdminDataAccess,
    ContactDataAccess,
    ContactService,
    CategoryDataAccess,
    Jwt,
    Tools,
    ConvertDate,
  ],
})
export class ContactModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes('admin/contactUs');
  }
}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { ContactDataAccess } from 'src/dataAccess/contactUs.dataAccess';
import {
  AdminAnswerToContactDto,
  ContactDto,
  contactObj,
  CreateContactDto,
} from 'src/DTO/contactUs.dto';

@Injectable()
export class ContactService {
  constructor(
    private readonly contactDataAccess: ContactDataAccess,
    private readonly categoryDataAccess: CategoryDataAccess,
  ) {}

  //  contact list *************************************
  async contactList(): Promise<ContactDto[]> {
    const contacts = await this.contactDataAccess.AdminFindAllContact();
    const contactList = contacts.map((key) => contactObj(key));
    return contactList;
  }

  // show contact info  ************************************
  async showContactInfo(contactId): Promise<ContactDto> {
    const contact = await this.contactDataAccess.AdminShowContactInfo(
      contactId,
    );
    return contactObj(contact);
  }

  // answer to contact  **************************************
  async answerToMessage(
    answerToContactDto: AdminAnswerToContactDto,
  ): Promise<ContactDto> {
    const { id, answer } = answerToContactDto;
    const Message = await this.contactDataAccess.AdminShowContactInfo(id);
    if (!Message) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'not found any message',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.contactDataAccess.AdminAnswerToContact(Message.id, answer);
    const newAnswer = await this.contactDataAccess.AdminShowContactInfo(
      Message.id,
    );
    return contactObj(newAnswer);
  }
}

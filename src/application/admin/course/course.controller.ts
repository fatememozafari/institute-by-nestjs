import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Param,
  Query,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import {
  AdminCreateCourseDto,
  AdminUpdateCourseDto,
  CourseDto,
  FilterCourseDto,
} from 'src/DTO/course.dto';
import { CourseService } from './course.service';

@ApiTags('admin/courses')
@Controller('course')
export class CourseController {
  constructor(private readonly courseService: CourseService) {}

  //*************************************** create course ****************************************************
  @Post('')
  @ApiOperation({ summary: 'register new course ' })
  @ApiOkResponse({
    description: 'course info',
    type: [CourseDto],
  })
  @ApiNotFoundResponse({ description: 'create filed' })
  @ApiInternalServerErrorResponse({
    description: 'create faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async adminCreateNewCourse(
    @Res() res: Response,
    @Body() createCourseDto: AdminCreateCourseDto,
  ): Promise<CourseDto> {
    try {
      const course = await this.courseService.adminCreateNewCourse(
        createCourseDto,
        // res.locals.user.id,
      );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** update course ********************************
  @ApiOperation({ summary: 'update course' })
  @ApiOkResponse({
    description: 'update course',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: 'course not found' })
  @ApiInternalServerErrorResponse({
    description: 'course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Put('')
  async updateCourse(
    @Res() res: Response,
    @Body() updatecourseDto: AdminUpdateCourseDto,
  ): Promise<CourseDto> {
    try {
      const course = await this.courseService.updateCourses(updatecourseDto);
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }
  //***************************************** filter *************************************** */
  @ApiOperation({ summary: 'filter  course ' })
  @ApiOkResponse({
    description: '  course list',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: ' course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get list faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('filter')
  async filterCourse(
    @Res() res: Response,
    @Query() filterCourse: FilterCourseDto,
  ): Promise<CourseDto[]> {
    try {
      const filter = await this.courseService.filterCourse(filterCourse);
      res.status(200).json(filter);
      return;
    } catch (err) {
      throw err;
    }
  }
}

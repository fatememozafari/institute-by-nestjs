import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { Jwt } from 'src/common/helpers/jwt.helper';
import { Tools } from 'src/common/helpers/tools.helper';
import { ConvertDate } from 'src/common/helpers/convertDate.helper';
import { ValidUserMiddleware } from 'src/common/middlewares/validateUser.middleware';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';
import { ValidAdminMiddleware } from 'src/common/middlewares/validateAdmin.middleware';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { CourseController } from './course.controller';
import { CourseDataAccess } from 'src/dataAccess/course.dataAccess';
import { CourseService } from './course.service';

@Module({
  imports: [],
  controllers: [CourseController],
  providers: [
    CourseDataAccess,
    CourseService,
    CategoryDataAccess,
    AdminDataAccess,
    Jwt,
    Tools,
    ConvertDate,
  ],
})
export class CourseModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes('admin/courses');
  }
}

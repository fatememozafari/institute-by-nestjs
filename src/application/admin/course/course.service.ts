import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserDataAccess } from '../../../dataAccess/user.dataAccess';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import courseTypes from 'src/common/eNums/courseTypes.enum';
import { CourseDataAccess } from 'src/dataAccess/course.dataAccess';
import {
  AdminCreateCourseDto,
  CourseDto,
  courseObj,
  FilterCourseDto,
} from 'src/DTO/course.dto';
import { AdminUpdateCourseDto } from 'src/DTO/course.dto';

@Injectable()
export class CourseService {
  constructor(
    private readonly courseDataAccess: CourseDataAccess,
    private readonly categoryDataAccess: CategoryDataAccess,
  ) {}
  // create  course **********************************
  async adminCreateNewCourse(
    createCourseDto: AdminCreateCourseDto,
    // userId,
  ): Promise<CourseDto> {
    const {
      title,
      slug,
      courseType,
      startDate,
      endDate,
      duration,
      // rate,
      teacher,
      categoryId,
    } = createCourseDto;
    const ckeckCategory = await this.categoryDataAccess.findCategoryById(
      categoryId,
    );
    if (!ckeckCategory) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'دپارتمان وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const course = await this.courseDataAccess.adminCreateNewCourse(
      title,
      slug,
      courseType,
      startDate,
      endDate,
      duration,
      // rate,
      teacher,
      categoryId,
      // userId,
    );
    const newCourse = await this.courseDataAccess.showCourseInfo(course.id);
    return courseObj(newCourse);
  }

  // update  Course **************************************
  async updateCourses(
    updatecourseDto: AdminUpdateCourseDto,
  ): Promise<CourseDto> {
    const { id, title, slug, status, startDate, endDate, duration } =
      updatecourseDto;

    const course = await this.courseDataAccess.findOneCourse(id);
    if (!course) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'not found any course',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.courseDataAccess.adminUpdateCourse(
      course.id,
      title,
      slug,
      status,
      startDate,
      endDate,
      duration,
    );
    const newCourse = await this.courseDataAccess.findOneCourse(course.id);
    return courseObj(newCourse);
  }

  //************************************** filter ************************************** */

  async filterCourse(filterCourseDto: FilterCourseDto): Promise<CourseDto[]> {
    const { title, courseType, categoryId, startDate, endDate } =
      filterCourseDto;
    const courses = await this.courseDataAccess.filterCourse(
      title,
      courseType,
      categoryId,
      startDate,
      endDate,
    );
    const courseList = courses.map((key) => courseObj(key));
    return courseList;
  }
}

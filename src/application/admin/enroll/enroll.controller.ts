import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Param,
  HttpException,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { EnrollDataAccess } from 'src/dataAccess/enroll.dataAccess';
import { EnrollDto, enrollObj, ScoreToCourseDto } from 'src/DTO/enroll.dto';
import { EnrollService } from './enroll.service';

@ApiTags('admin/enrolls')
@Controller('enroll')
export class EnrollController {
  constructor(
    private readonly enrollService: EnrollService,
    private readonly enrollDataAccess: EnrollDataAccess,
  ) {}

  //***************************************  enroll list ************************************
  @ApiOperation({ summary: 'get  my enroll list' })
  @ApiOkResponse({
    description: '  my enroll list',
    type: EnrollDto,
  })
  @ApiNotFoundResponse({ description: ' my enroll not found' })
  @ApiInternalServerErrorResponse({
    description: ' get  my enrolls faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('enrollList')
  async EnrollList(@Res() res: Response): Promise<EnrollDto[]> {
    try {
      const enrolls = await this.enrollService.EnrollList();
      res.status(200).json(enrolls);
      return;
    } catch (err) {
      throw err;
    }
  }

  // take score to course  **************************************
  @ApiOperation({ summary: 'take score to course' })
  @ApiOkResponse({
    description: 'take score to course',
    type: EnrollDto,
  })
  @ApiNotFoundResponse({ description: 'course not found' })
  @ApiInternalServerErrorResponse({
    description: 'take score faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Put('')
  async scoreToCourse(
    @Res() res: Response,
    @Body() scoreToCourse: ScoreToCourseDto,
  ): Promise<EnrollDto> {
    try {
      const score = await this.enrollService.scoreToCourse(scoreToCourse);
      res.status(200).json(score);
      return;
    } catch (err) {
      throw err;
    }
  }
}

import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';

import { Jwt } from 'src/common/helpers/jwt.helper';
import { Tools } from 'src/common/helpers/tools.helper';
import { ConvertDate } from 'src/common/helpers/convertDate.helper';
import { EnrollController } from './enroll.controller';
import { EnrollDataAccess } from 'src/dataAccess/enroll.dataAccess';
import { EnrollService } from './enroll.service';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';
import { ValidAdminMiddleware } from 'src/common/middlewares/validateAdmin.middleware';
import { UserDataAccess } from 'src/dataAccess/user.dataAccess';

@Module({
  imports: [],
  controllers: [EnrollController],
  providers: [
    EnrollDataAccess,
    EnrollService,
    AdminDataAccess,
    Jwt,
    Tools,
    ConvertDate,
  ],
})
export class EnrollModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes('admin/enrolls');
  }
}

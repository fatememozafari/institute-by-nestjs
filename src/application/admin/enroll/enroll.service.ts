import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { EnrollDataAccess } from 'src/dataAccess/enroll.dataAccess';
import { EnrollDto, enrollObj, ScoreToCourseDto } from 'src/DTO/enroll.dto';

@Injectable()
export class EnrollService {
  constructor(private readonly enrollDataAccess: EnrollDataAccess) {}
  //  enroll list ************************************
  async EnrollList(): Promise<EnrollDto[]> {
    const enrolls = await this.enrollDataAccess.AdminFindAllEnroll();
    if (!enrolls) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'هیچ ثبتنامی انجام نشده است.',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const enrollList = enrolls.map((key) => enrollObj(key));
    return enrollList;
  }
  // take score to course **************************************
  async scoreToCourse(scoreToCourseDto: ScoreToCourseDto): Promise<EnrollDto> {
    const { score, courseId, userId } = scoreToCourseDto;
    const enroll = await this.enrollDataAccess.findOneCourse(courseId, userId);
    if (!enroll) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'not found any enroll',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.enrollDataAccess.adminUpdateScore(score, courseId, userId);
    const newScore = await this.enrollDataAccess.ShowEnrollInfo(enroll.id);
    return enrollObj(newScore);
  }
}

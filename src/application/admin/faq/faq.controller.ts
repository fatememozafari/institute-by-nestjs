import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AdminCreateFaqDto, FaqDto } from 'src/DTO/faq.dto';
import { FaqService } from './faq.service';

@ApiTags('admin/faqs')
@Controller('faq')
export class FaqController {
  constructor(private readonly faqService: FaqService) {}
  //*************************************** question list ************************************
  @ApiOperation({ summary: 'get question list' })
  @ApiOkResponse({
    description: ' question list',
    type: FaqDto,
  })
  @ApiNotFoundResponse({ description: 'question not found' })
  @ApiInternalServerErrorResponse({
    description: ' get list faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('')
  async questionList(@Res() res: Response): Promise<FaqDto> {
    try {
      const question = await this.faqService.QuestionList();
      res.status(200).json(question);
      return;
    } catch (err) {
      throw err;
    }
  }
  //*************************************** create new question ****************************************************
  @Post('')
  @ApiOperation({ summary: 'create new question ' })
  @ApiOkResponse({
    description: 'question info',
    type: FaqDto,
  })
  @ApiNotFoundResponse({ description: 'create filed' })
  @ApiInternalServerErrorResponse({
    description: 'create faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async createNewQuestion(
    @Res() res: Response,
    @Body() createFaqDto: AdminCreateFaqDto,
  ): Promise<FaqDto> {
    try {
      const question = await this.faqService.createNewFaq(createFaqDto);
      res.status(200).json(question);
      return;
    } catch (err) {
      throw err;
    }
  }
  //*************************************** delete faq ************************************
  @Delete(':faqId')
  @ApiOperation({ summary: 'delete faq ' })
  @ApiOkResponse({
    description: 'delete faq',
    type: FaqDto,
  })
  @ApiNotFoundResponse({ description: 'delete filed' })
  @ApiInternalServerErrorResponse({
    description: 'delete faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async deleteFaq(
    @Res() res: Response,
    @Param('faqId') faqId: number,
  ): Promise<FaqDto> {
    try {
      await this.faqService.deleteFaq(faqId);
      res.status(200).json(true);
      return;
    } catch (err) {
      throw err;
    }
  }
}

import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { ConvertDate } from 'src/common/helpers/convertDate.helper';
import { Jwt } from 'src/common/helpers/jwt.helper';
import { Tools } from 'src/common/helpers/tools.helper';
import { ValidAdminMiddleware } from 'src/common/middlewares/validateAdmin.middleware';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';
import { FaqDataAccess } from 'src/dataAccess/faq.dataAccess';
import { FaqController } from './faq.controller';
import { FaqService } from './faq.service';

@Module({
  imports: [],
  controllers: [FaqController],
  providers: [
    FaqDataAccess,
    FaqService,
    AdminDataAccess,
    Jwt,
    Tools,
    ConvertDate,
  ],
})
export class FaqModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes('admin/faqs');
  }
}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FaqDataAccess } from 'src/dataAccess/faq.dataAccess';
import { AdminCreateFaqDto, FaqDto, faqObj } from 'src/DTO/faq.dto';

@Injectable()
export class FaqService {
  constructor(private readonly faqDataAccess: FaqDataAccess) {}
  // question list ************************************
  async QuestionList(): Promise<FaqDto[]> {
    const questions = await this.faqDataAccess.findAllFaq();
    const questionList = questions.map((key) => faqObj(key));
    return questionList;
  }
  // create new question **********************************
  async createNewFaq(createFaqDto: AdminCreateFaqDto): Promise<FaqDto> {
    const { question, answer, file, categoryId } = createFaqDto;
    const newFaq = await this.faqDataAccess.AdminCreateFaq(
      question,
      answer,
      file,
      categoryId,
    );
    const result = await this.faqDataAccess.showFaqInfo(newFaq.id);
    return faqObj(result);
  }
  //delete one question ************************************
  async deleteFaq(faqId): Promise<any> {
    await this.faqDataAccess.adminDeleteFaq(faqId);
    return;
  }
}

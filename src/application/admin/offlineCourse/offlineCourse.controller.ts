import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Param,
  Query,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import {
  AdminCreateOfflineCourseDto,
  AdminUpdateOfflineCourseDto,
  FilterOfflineCourseDto,
  OfflineCourseDto,
} from 'src/DTO/offlineCourse.dto';
import { OfflineCourseService } from './offlineCourse.service';

@ApiTags('admin/offlineCourses')
@Controller('offlineCourse')
export class OfflineCourseController {
  constructor(private readonly offlineService: OfflineCourseService) {}

  //*************************************** create course ****************************************************
  @Post('')
  @ApiOperation({ summary: 'create new course ' })
  @ApiOkResponse({
    description: 'course info',
    type: [OfflineCourseDto],
  })
  @ApiNotFoundResponse({ description: 'create filed' })
  @ApiInternalServerErrorResponse({
    description: 'create faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async createNewCourse(
    @Res() res: Response,
    @Body() createCourseDto: AdminCreateOfflineCourseDto,
  ): Promise<OfflineCourseDto> {
    try {
      const course = await this.offlineService.adminCreateNewCourse(
        createCourseDto,
        // res.locals.admin.id,
      );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** update course ********************************
  @ApiOperation({ summary: 'update course' })
  @ApiOkResponse({
    description: 'update course',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'course not found' })
  @ApiInternalServerErrorResponse({
    description: 'course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Put('')
  async updateCourse(
    @Res() res: Response,
    @Body() updatecourseDto: AdminUpdateOfflineCourseDto,
  ): Promise<OfflineCourseDto> {
    try {
      const course = await this.offlineService.adminUpdateOfflineCourse(
        updatecourseDto,
      );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }
  //***************************************** filter *************************************** */
  @ApiOperation({ summary: 'filter offline course ' })
  @ApiOkResponse({
    description: ' offline course list',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get list faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('filter')
  async filterOfflineCourse(
    @Res() res: Response,
    @Query() filterOfflineCourse: FilterOfflineCourseDto,
  ): Promise<OfflineCourseDto[]> {
    try {
      const filter = await this.offlineService.filterCourse(
        filterOfflineCourse,
      );
      res.status(200).json(filter);
      return;
    } catch (err) {
      throw err;
    }
  }
}

import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';


import { Jwt } from 'src/common/helpers/jwt.helper';
import { Tools } from 'src/common/helpers/tools.helper';
import { ConvertDate } from 'src/common/helpers/convertDate.helper';
import { ValidUserMiddleware } from 'src/common/middlewares/validateUser.middleware';
import { OfflineCourseController } from './offlineCourse.controller';
import { OfflineCourseDataAccess } from 'src/dataAccess/offlineCourse.dataAccess';
import { OfflineCourseService } from './offlineCourse.service';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';
import { ValidAdminMiddleware } from 'src/common/middlewares/validateAdmin.middleware';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';

@Module({
  imports: [],
  controllers: [OfflineCourseController],
  providers: [
    OfflineCourseDataAccess,
    OfflineCourseService,
    CategoryDataAccess,
    AdminDataAccess,
    Jwt,
    Tools,
    ConvertDate,
  ],
})
export class OfflineCourseModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidAdminMiddleware).forRoutes('admin/offlineCourses');
  }
}

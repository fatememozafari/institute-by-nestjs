import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserDataAccess } from '../../../dataAccess/user.dataAccess';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { OfflineCourseDataAccess } from 'src/dataAccess/offlineCourse.dataAccess';
import {
  AdminCreateOfflineCourseDto,
  AdminUpdateOfflineCourseDto,
  FilterOfflineCourseDto,
  OfflineCourseDto,
  offlineCourseObj,
} from 'src/DTO/offlineCourse.dto';
import courseTypes from 'src/common/eNums/courseTypes.enum';

@Injectable()
export class OfflineCourseService {
  constructor(
    private readonly offlineDataAccess: OfflineCourseDataAccess,
    private readonly categoryDataAccess: CategoryDataAccess,
  ) {}
  // create offline course **********************************
  async adminCreateNewCourse(
    createCourseDto: AdminCreateOfflineCourseDto,
    // adminId,
  ): Promise<OfflineCourseDto> {
    const {
      title,
      slug,
      courseType,
      // status,
      duration,
      // rate,
      teacher,
      categoryId,
      description,
      file,
    } = createCourseDto;
    const ckeckCategory = await this.categoryDataAccess.findCategoryById(
      categoryId,
    );
    if (!ckeckCategory) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'دپارتمان وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const course = await this.offlineDataAccess.adminCreateOfflineCourse(
      title,
      slug,
      courseType,
      // status,
      duration,
      // rate,
      teacher,
      categoryId,
      description,
      file,
      // adminId,
    );
    const newCourse = await this.offlineDataAccess.showOfflineCourseInfo(
      course.id,
    );
    return offlineCourseObj(newCourse);
  }

  // update offline Course **************************************
  async adminUpdateOfflineCourse(
    updatecourseDto: AdminUpdateOfflineCourseDto,
  ): Promise<OfflineCourseDto> {
    const { id, title, slug, status, duration, description, file } =
      updatecourseDto;

    const course = await this.offlineDataAccess.findOneOfflineCourse(id);
    if (!course) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'not found any course',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    await this.offlineDataAccess.adminUpdateOfflineCourse(
      course.id,
      title,
      slug,
      status,
      duration,
      description,
      file,
    );
    const newCourse = await this.offlineDataAccess.findOneOfflineCourse(
      course.id,
    );
    return offlineCourseObj(newCourse);
  }

  //************************************** filter ************************************** */

  async filterCourse(
    filterCourseDto: FilterOfflineCourseDto,
  ): Promise<OfflineCourseDto[]> {
    const { title, courseType, categoryId } = filterCourseDto;
    const courses = await this.offlineDataAccess.filterOfflineCourse(
      title,
      courseType,
      categoryId,
    );
    const courseList = courses.map((key) => offlineCourseObj(key));
    return courseList;
  }
}

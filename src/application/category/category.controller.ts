import { Controller, HttpStatus, HttpCode, Res, Get } from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { CategoryService } from './category.service';

import { CategoryDto } from '../../DTO/category.dto';

@ApiTags('categories')
@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}
  // get category inf ********************************
  @ApiOperation({ summary: 'get category inf' })
  @ApiOkResponse({
    description: ' category inf',
    type: CategoryDto,
  })
  @ApiNotFoundResponse({ description: 'category not found' })
  @ApiInternalServerErrorResponse({
    description: ' get inf faild',
  })
  @HttpCode(HttpStatus.OK)
  @Get('')
  async categoryList(@Res() res: Response): Promise<CategoryDto> {
    try {
      const category = await this.categoryService.categoryList();
      res.status(200).json(category);
      return;
    } catch (err) {
      throw err;
    }
  }
}

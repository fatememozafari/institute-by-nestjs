import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';

@Module({
  imports: [],
  controllers: [CategoryController],
  providers: [CategoryService, CategoryDataAccess],
})
export class CategoryModule {}

import { Injectable } from '@nestjs/common';
import { CategoryDto, categoryObj } from '../../DTO/category.dto';

import { CategoryDataAccess } from '../../dataAccess/category.dataAccess';

@Injectable()
export class CategoryService {
  constructor(private readonly categoryDataAccess: CategoryDataAccess) {}
  // show category ************************************
  async categoryList(): Promise<CategoryDto[]> {
    const categories = await this.categoryDataAccess.findAllCategory();
    const categoryList = categories.map((category) => categoryObj(category));
    return categoryList;
  }
}

import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Param,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { AdminCreateFaqDto, FaqDto } from 'src/DTO/faq.dto';
import { FaqService } from './faq.service';

@ApiTags('faq')
@Controller('faq')
export class FaqController {
  constructor(private readonly faqService: FaqService) {}
  //*************************************** question list ************************************
  @ApiOperation({ summary: 'get question list' })
  @ApiOkResponse({
    description: ' question list',
    type: FaqDto,
  })
  @ApiNotFoundResponse({ description: 'question not found' })
  @ApiInternalServerErrorResponse({
    description: ' get list faild',
  })
  @HttpCode(HttpStatus.OK)
  @Get('')
  async questionList(@Res() res: Response): Promise<FaqDto> {
    try {
      const question = await this.faqService.QuestionList();
      res.status(200).json(question);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** show question info ************************************
  @ApiOkResponse({
    description: 'find successfully',
    type: FaqDto,
  })
  @ApiOperation({ summary: 'show question info' })
  @ApiNotFoundResponse({ description: 'question not found' })
  @ApiInternalServerErrorResponse({
    description: 'question faild',
  })
  @Get('detail/:faqId')
  @HttpCode(HttpStatus.OK)
  async showQuestionInfo(
    @Res() res: Response,
    @Param('faqId') faqId: number,
  ): Promise<FaqDto> {
    try {
      const question = await this.faqService.showQuestionInfo(faqId);
      res.status(200).json(question);
      return;
    } catch (err) {
      throw err;
    }
  }
}

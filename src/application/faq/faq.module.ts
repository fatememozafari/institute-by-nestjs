import { Module } from '@nestjs/common';
import { FaqDataAccess } from 'src/dataAccess/faq.dataAccess';
import { FaqController } from './faq.controller';
import { FaqService } from './faq.service';

@Module({
  imports: [],
  controllers: [FaqController],
  providers: [FaqService, FaqDataAccess],
})
export class FaqModule {}

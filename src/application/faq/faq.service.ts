import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FaqDataAccess } from 'src/dataAccess/faq.dataAccess';
import { AdminCreateFaqDto, FaqDto, faqObj } from 'src/DTO/faq.dto';

@Injectable()
export class FaqService {
  constructor(private readonly faqDataAccess: FaqDataAccess) {}
  // question list ************************************
  async QuestionList(): Promise<FaqDto[]> {
    const questions = await this.faqDataAccess.findAllFaq();
    const questionList = questions.map((key) => faqObj(key));
    return questionList;
  }
  // question info ************************************
  async showQuestionInfo(faqId): Promise<FaqDto> {
    const question = await this.faqDataAccess.showFaqInfo(faqId);
    if (!question) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'question not found',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    return faqObj(question);
  }
}

import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Param,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { ContactDto, CreateContactDto } from 'src/DTO/contactUs.dto';
import { ContactService } from './contact.sevice';

@ApiTags('contactUs')
@Controller('contactUs')
export class ContactController {
  constructor(private readonly contactService: ContactService) {}
  //*********************** create new contact*********************** */
  @Post('')
  @ApiOperation({ summary: ' create new contact' })
  @ApiOkResponse({
    description: ' new contact ',
    type: ContactDto,
  })
  @ApiNotFoundResponse({ description: ' contact not found' })
  @ApiInternalServerErrorResponse({
    description: 'create new contact failed',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async createNewContact(
    @Res() res: Response,
    @Body() createContactDto: CreateContactDto,
  ): Promise<ContactDto> {
    try {
      const contact = await this.contactService.createNewContact(
        createContactDto,
        res.locals.user.id,
      );
      res.status(200).json(contact);
      return;
    } catch (err) {
      throw err;
    }
  }
}

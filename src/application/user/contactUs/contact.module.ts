import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { ConvertDate } from 'src/common/helpers/convertDate.helper';
import { Jwt } from 'src/common/helpers/jwt.helper';
import { Tools } from 'src/common/helpers/tools.helper';
import { ValidUserMiddleware } from 'src/common/middlewares/validateUser.middleware';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { ContactDataAccess } from 'src/dataAccess/contactUs.dataAccess';
import { UserDataAccess } from 'src/dataAccess/user.dataAccess';
import { ContactController } from './contact.controller';
import { ContactService } from './contact.sevice';

@Module({
  imports: [],
  controllers: [ContactController],
  providers: [
    UserDataAccess,
    ContactDataAccess,
    ContactService,
    CategoryDataAccess,
    Jwt,
    Tools,
    ConvertDate,
  ],
})
export class ContactModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidUserMiddleware).forRoutes('user/contactUs');
  }
}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { ContactDataAccess } from 'src/dataAccess/contactUs.dataAccess';
import { UserDataAccess } from 'src/dataAccess/user.dataAccess';
import { Contact } from 'src/models';
import { CreateContactDto } from 'src/DTO/contactUs.dto';
import { contactObj } from 'src/DTO/contactUs.dto';

@Injectable()
export class ContactService {
  constructor(
    private readonly contactDataAccess: ContactDataAccess,
    private readonly categoryDataAccess: CategoryDataAccess,
    private readonly userDataAccess: UserDataAccess,
  ) {}

  // create a new contact *************************************
  async createNewContact(
    createContactDto: CreateContactDto,
    userId,
  ): Promise<any> {
    const { name, title, message, file, categoryId } = createContactDto;
    const checkCategory = await this.categoryDataAccess.findCategoryById(
      categoryId,
    );
    if (!checkCategory) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'این دسته بندی وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const contact = await this.contactDataAccess.createContact(
      name,
      title,
      message,
      file,
      categoryId,
      userId,
    );
    const newContact = await this.contactDataAccess.AdminShowContactInfo(
      contact.id,
    );
    return contactObj(newContact);
  }
}

import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Param,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { CourseDto, FilterCourseDto } from 'src/DTO/course.dto';
import { CourseService } from './course.service';

@ApiTags('course')
@Controller('course')
export class CourseController {
  constructor(private readonly courseService: CourseService) {}
  //***************************************  course list ************************************
  @ApiOperation({ summary: 'get  course list' })
  @ApiOkResponse({
    description: '  course list',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: ' course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get  course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('allCourse')
  async courseList(@Res() res: Response): Promise<CourseDto[]> {
    try {
      const course = await this.courseService.courseList();
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //***************************************  course list by categoryId************************************
  @ApiOperation({ summary: 'get  course list by categoryId' })
  @ApiOkResponse({
    description: '  course list',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: ' course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get  course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('courseList/:categoryId')
  async courseListByCategory(
    @Res() res: Response,
    @Param('categoryId') categoryId: number,
  ): Promise<CourseDto[]> {
    try {
      const course = await this.courseService.courseListByCategoryId(
        categoryId,
      );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }
  //***************************************  course list by course type************************************
  @ApiOperation({ summary: 'get  course list by course type' })
  @ApiOkResponse({
    description: '  course list',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: ' course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get  course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('allCourse/:courseType')
  async courseListByCourseType(
    @Res() res: Response,
    @Param('courseType') courseType: number,
  ): Promise<CourseDto[]> {
    try {
      const course = await this.courseService.courseListByCourseType(
        courseType,
      );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //***************************************  course list by course id************************************
  @ApiOperation({ summary: 'get  course list by course id' })
  @ApiOkResponse({
    description: '  course list',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: ' course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get  course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('course/:courseId')
  async courseListByCourseId(
    @Res() res: Response,
    @Param('courseId') courseId: number,
  ): Promise<CourseDto> {
    try {
      const course = await this.courseService.courseByCourseId(courseId);
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //***************************************  course list by title************************************
  @ApiOperation({ summary: 'get  course list by title' })
  @ApiOkResponse({
    description: '  course list',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: ' course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get  course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('oneCourse/:title')
  async courseListByTitle(
    @Res() res: Response,
    @Param('title') title: string,
  ): Promise<CourseDto> {
    try {
      const course = await this.courseService.courseByTitle(title);
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //***************************************** filter *************************************** */
  @ApiOperation({ summary: 'filter offline course ' })
  @ApiOkResponse({
    description: ' offline course list',
    type: CourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get list faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('filter')
  async filterCourse(
    @Res() res: Response,
    @Query() filterCourse: FilterCourseDto,
  ): Promise<CourseDto[]> {
    try {
      const filter = await this.courseService.filterCourse(filterCourse);
      res.status(200).json(filter);
      return;
    } catch (err) {
      throw err;
    }
  }
}

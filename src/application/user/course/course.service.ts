import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserDataAccess } from '../../../dataAccess/user.dataAccess';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { CourseDataAccess } from 'src/dataAccess/course.dataAccess';
import { CourseDto, courseObj, FilterCourseDto } from 'src/DTO/course.dto';

@Injectable()
export class CourseService {
  constructor(
    private readonly courseDataAccess: CourseDataAccess,
    private readonly categoryDataAccess: CategoryDataAccess,
  ) {}
  //  course list ************************************
  async courseList(): Promise<CourseDto[]> {
    const courses = await this.courseDataAccess.findAllCourse();
    const courseList = courses.map((key) => courseObj(key));
    return courseList;
  }
  // course list by categoryId ************************************
  async courseListByCategoryId(categoryId): Promise<CourseDto[]> {
    const courses = await this.courseDataAccess.findAllCourseByCategory(
      categoryId,
    );
    if (!courses) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'دسته بندی وارد شده وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const courseList = courses.map((key) => courseObj(key));
    return courseList;
  }
  // course list by course type ************************************
  async courseListByCourseType(courseType): Promise<CourseDto[]> {
    const courses = await this.courseDataAccess.findAllCourseByCourseType(
      courseType,
    );
    if (!courses) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'موضوع وارد شده وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const courseList = courses.map((key) => courseObj(key));
    return courseList;
  }
  // course by course id ************************************
  async courseByCourseId(courseId): Promise<CourseDto> {
    const course = await this.courseDataAccess.findOneCourse(courseId);
    if (!course) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'درس وارد شده وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const findCourse = courseObj(course);
    return findCourse;
  }
  // course by title ************************************
  async courseByTitle(title): Promise<CourseDto> {
    const course = await this.courseDataAccess.findOneCourseByTitle(title);
    if (!course) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'درس وارد شده وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const findCourse = courseObj(course);
    return findCourse;
  }

  //************************************** filter ************************************** */

  async filterCourse(filterDto: FilterCourseDto): Promise<CourseDto[]> {
    const { title, courseType, categoryId, startDate, endDate } = filterDto;
    const courses = await this.courseDataAccess.filterCourse(
      title,
      courseType,
      categoryId,
      startDate,
      endDate,
      // 10,
      // 0,
    );
    const courseList = courses.map((key) => courseObj(key));
    return courseList;
  }
}

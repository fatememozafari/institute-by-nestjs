import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Param,
  Delete,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { CourseDto, FilterCourseDto } from 'src/DTO/course.dto';
import { CreateEnrollDto, EnrollDto } from 'src/DTO/enroll.dto';
import { EnrollService } from './enroll.service';

@ApiTags('enroll')
@Controller('enroll')
export class EnrollController {
  constructor(private readonly enrollService: EnrollService) {}
  // *************************************enroll in course ********************************
  @Post('')
  @ApiOperation({ summary: 'create new enroll ' })
  @ApiOkResponse({
    description: 'enroll info',
    type: EnrollDto,
  })
  @ApiNotFoundResponse({ description: 'create failed' })
  @ApiInternalServerErrorResponse({
    description: 'create faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async createNewDepartment(
    @Res() res: Response,
    @Body() createEnrollDto: CreateEnrollDto,
  ): Promise<EnrollDto> {
    try {
      const enroll = await this.enrollService.createEnroll(
        createEnrollDto,
        res.locals.user.id,
      );
      res.status(200).json(enroll);
      return;
    } catch (err) {
      throw err;
    }
  }
  //*************************************** my enroll list ************************************
  @ApiOperation({ summary: 'get  my enroll list' })
  @ApiOkResponse({
    description: '  my enroll list',
    type: EnrollDto,
  })
  @ApiNotFoundResponse({ description: ' my enroll not found' })
  @ApiInternalServerErrorResponse({
    description: ' get  my enrolls faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('myEnroll')
  async myEnrollList(@Res() res: Response): Promise<EnrollDto[]> {
    try {
      const enrolls = await this.enrollService.findMyEnrolles(
        res.locals.user.id,
      );
      res.status(200).json(enrolls);
      return;
    } catch (err) {
      throw err;
    }
  }
  //*************************************** delete enroll ************************************
  @Delete(':enrollId')
  @ApiOperation({ summary: 'delete enroll ' })
  @ApiOkResponse({
    description: 'delete enroll',
    type: EnrollDto,
  })
  @ApiNotFoundResponse({ description: 'delete filed' })
  @ApiInternalServerErrorResponse({
    description: 'delete faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  async deleteFaq(
    @Res() res: Response,
    @Param('enrollId') enrollId: number,
  ): Promise<EnrollDto> {
    try {
      await this.enrollService.deleteEnroll(enrollId, res.locals.user.id);
      res.status(200).json(true);
      return;
    } catch (err) {
      throw err;
    }
  }
}

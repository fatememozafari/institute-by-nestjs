import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserDataAccess } from '../../../dataAccess/user.dataAccess';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { CourseDataAccess } from 'src/dataAccess/course.dataAccess';
import { CourseDto, courseObj, FilterCourseDto } from 'src/DTO/course.dto';
import { EnrollDataAccess } from 'src/dataAccess/enroll.dataAccess';
import { CreateEnrollDto, EnrollDto, enrollObj } from 'src/DTO/enroll.dto';

@Injectable()
export class EnrollService {
  constructor(private readonly enrollDataAccess: EnrollDataAccess) {}
  //  enroll in course ************************************
  async createEnroll(
    createEnrollDto: CreateEnrollDto,
    userId,
  ): Promise<EnrollDto> {
    const { courseId } = createEnrollDto;
    const checkEnroll = await this.enrollDataAccess.findOneCourse(
      courseId,
      userId,
    );
    if (checkEnroll) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'قبلا در این درس ثبت نام انجام داده اید.',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const enroll = await this.enrollDataAccess.CreateEnroll(courseId, userId);
    const newEnroll = await this.enrollDataAccess.ShowEnrollInfo(enroll.id);

    return enrollObj(newEnroll);
  }
  //  my enroll list ************************************
  async findMyEnrolles(userId): Promise<EnrollDto[]> {
    const enrolls = await this.enrollDataAccess.findAllEnroll(userId);
    if (!enrolls) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'هیچ ثبتنامی انجام نشده است.',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const enrollList = enrolls.map((key) => enrollObj(key));
    return enrollList;
  }
  //delete one enroll ************************************
  async deleteEnroll(enrollId, userId): Promise<any> {
    await this.enrollDataAccess.DeleteEnroll(enrollId, userId);
    return;
  }
}

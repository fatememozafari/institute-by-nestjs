import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Query,
  Param,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Response } from 'express';
import { FilterOfflineCourseDto } from 'src/DTO/offlineCourse.dto';
import { OfflineCourseDto } from 'src/DTO/offlineCourse.dto';
import { OfflineCourseService } from './offlineCourse.service';

@ApiTags('offlineCourse')
@Controller('offlineCourse')
export class OfflineCourseController {
  constructor(private readonly offlineCourseService: OfflineCourseService) {}
  //*************************************** offline course list ************************************
  @ApiOperation({ summary: 'get offline course list' })
  @ApiOkResponse({
    description: ' offline course list',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get offline course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('allOfflineCourse')
  async offlineCourseList(@Res() res: Response): Promise<OfflineCourseDto[]> {
    try {
      const course = await this.offlineCourseService.offlineCourseList();
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** offline course list by categoryId************************************
  @ApiOperation({ summary: 'get offline course list by categoryId' })
  @ApiOkResponse({
    description: ' offline course list',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get offline course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('offlineCourseList/:categoryId')
  async offlineCourseListByCategory(
    @Res() res: Response,
    @Param('categoryId') categoryId: number,
  ): Promise<OfflineCourseDto[]> {
    try {
      const course =
        await this.offlineCourseService.offlineCourseListByCategoryId(
          categoryId,
        );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }
  //*************************************** offline course list by course type************************************
  @ApiOperation({ summary: 'get offline course list by course type' })
  @ApiOkResponse({
    description: ' offline course list',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get offline course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('allOfflineCourse/:courseType')
  async offlineCourseListByCourseType(
    @Res() res: Response,
    @Param('courseType') courseType: number,
  ): Promise<OfflineCourseDto[]> {
    try {
      const course =
        await this.offlineCourseService.offlineCourseListByCourseType(
          courseType,
        );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** offline course list by course id************************************
  @ApiOperation({ summary: 'get offline course list by course id' })
  @ApiOkResponse({
    description: ' offline course list',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get offline course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('offlineCourse/:courseId')
  async offlineCourseListByCourseId(
    @Res() res: Response,
    @Param('courseId') courseId: number,
  ): Promise<OfflineCourseDto> {
    try {
      const course = await this.offlineCourseService.OfflineCourseByCourseId(
        courseId,
      );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //*************************************** offline course list by title************************************
  @ApiOperation({ summary: 'get offline course list by title' })
  @ApiOkResponse({
    description: ' offline course list',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get offline course faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('oneOfflineCourse/:title')
  async offlineCourseListByTitle(
    @Res() res: Response,
    @Param('title') title: string,
  ): Promise<OfflineCourseDto> {
    try {
      const course = await this.offlineCourseService.OfflineCourseByTitle(
        title,
      );
      res.status(200).json(course);
      return;
    } catch (err) {
      throw err;
    }
  }

  //***************************************** filter *************************************** */
  @ApiOperation({ summary: 'filter offline course ' })
  @ApiOkResponse({
    description: ' offline course list',
    type: OfflineCourseDto,
  })
  @ApiNotFoundResponse({ description: 'offline course not found' })
  @ApiInternalServerErrorResponse({
    description: ' get list faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('filter')
  async filterOfflineCourse(
    @Res() res: Response,
    @Query() filterOfflineCourse: FilterOfflineCourseDto,
  ): Promise<OfflineCourseDto[]> {
    try {
      const filter = await this.offlineCourseService.filterOfflineCourse(
        filterOfflineCourse,
      );
      res.status(200).json(filter);
      return;
    } catch (err) {
      throw err;
    }
  }
}

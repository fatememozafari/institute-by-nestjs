import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserDataAccess } from '../../../dataAccess/user.dataAccess';
import { CategoryDataAccess } from 'src/dataAccess/category.dataAccess';
import { OfflineCourseDataAccess } from 'src/dataAccess/offlineCourse.dataAccess';
import {
  FilterOfflineCourseDto,
  OfflineCourseDto,
  offlineCourseObj,
} from 'src/DTO/offlineCourse.dto';

@Injectable()
export class OfflineCourseService {
  constructor(
    private readonly offlineDataAccess: OfflineCourseDataAccess,
    private readonly categoryDataAccess: CategoryDataAccess,
  ) {}
  // offline course list ************************************
  async offlineCourseList(): Promise<OfflineCourseDto[]> {
    const courses = await this.offlineDataAccess.findAllOfflineCourse();
    const courseList = courses.map((key) => offlineCourseObj(key));
    return courseList;
  }
  //offline course list by categoryId ************************************
  async offlineCourseListByCategoryId(categoryId): Promise<OfflineCourseDto[]> {
    const courses = await this.offlineDataAccess.findAllOfflineCourseByCategory(
      categoryId,
    );
    if (!courses) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'دسته بندی وارد شده وجود ندارد',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const courseList = courses.map((key) => offlineCourseObj(key));
    return courseList;
  }
  //offline course list by course type ************************************
  async offlineCourseListByCourseType(courseType): Promise<OfflineCourseDto[]> {
    const courses =
      await this.offlineDataAccess.findAllOfflineCourseByCourseType(courseType);
    if (!courses) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'موضوع وارد شده وجود ندارد',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const courseList = courses.map((key) => offlineCourseObj(key));
    return courseList;
  }
  //offline course by course id ************************************
  async OfflineCourseByCourseId(courseId): Promise<OfflineCourseDto> {
    const course = await this.offlineDataAccess.findOneOfflineCourse(courseId);
    if (!course) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'درس وارد شده وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const findCourse = offlineCourseObj(course);
    return findCourse;
  }
  //offline course by title ************************************
  async OfflineCourseByTitle(title): Promise<OfflineCourseDto> {
    const course = await this.offlineDataAccess.findOneOfflineCourseByTitle(
      title,
    );
    if (!course) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'درس وارد شده وجود ندارد',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const findCourse = offlineCourseObj(course);
    return findCourse;
  }

  //************************************** filter ************************************** */

  async filterOfflineCourse(
    filterOfflineDto: FilterOfflineCourseDto,
  ): Promise<OfflineCourseDto[]> {
    const { title, courseType, categoryId } = filterOfflineDto;
    const courses = await this.offlineDataAccess.filterOfflineCourse(
      title,
      courseType,
      categoryId,
    );
    const courseList = courses.map((key) => offlineCourseObj(key));
    return courseList;
  }
}

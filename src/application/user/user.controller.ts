import {
  Body,
  Controller,
  Post,
  HttpStatus,
  HttpCode,
  Res,
  Get,
  Put,
  Req,
  Headers,
} from '@nestjs/common';
import {
  ApiTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiOperation,
  ApiHeader,
} from '@nestjs/swagger';
import { Request, Response } from 'express';
import { UserService } from './user.service';

import {
  CreateUserDto,
  UserDto,
  LoginUserDto,
  UpdateUserDto,
} from '../../DTO/user.dto';

@ApiTags('users')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}
  // user register ****************************************************
  @Post('register')
  @ApiOperation({ summary: 'register new user ' })
  @ApiOkResponse({
    description: 'user info',
    type: [UserDto],
  })
  @ApiNotFoundResponse({ description: 'register filed' })
  @ApiInternalServerErrorResponse({
    description: 'register faild',
  })
  async register(
    @Res() res: Response,
    @Body() createUserDto: CreateUserDto,
  ): Promise<UserDto> {
    try {
      if (res.locals && res.locals.user) {
        return res.locals.user;
      }
      const user = await this.userService.register(createUserDto);
      res.status(200).json(user);
      return;
    } catch (err) {
      throw err;
    }
  }
  // login ************************************
  @ApiOkResponse({
    description: 'user login successful',
    type: [UserDto],
  })
  @ApiOperation({ summary: 'Login registered user' })
  @ApiNotFoundResponse({ description: 'user not found' })
  @ApiInternalServerErrorResponse({
    description: 'user faild',
  })
  @Post('')
  @HttpCode(HttpStatus.OK)
  async login(
    @Res() res: Response,
    @Body() loginUserDto: LoginUserDto,
  ): Promise<UserDto> {
    try {
      if (res.locals && res.locals.user) {
        return res.locals.user;
      }
      const user = await this.userService.login(loginUserDto);
      res.status(200).json(user);
      return;
    } catch (err) {
      throw err;
    }
  }
  // logOut ********************************
  @ApiOperation({ summary: 'logOut user' })
  @ApiOkResponse({
    description: 'logOut',
    type: Boolean,
  })
  @ApiNotFoundResponse({ description: 'user not found' })
  @ApiInternalServerErrorResponse({
    description: 'logout faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('signOut')
  async signOut(
    @Headers() headers,
    @Res() res: Response,
    @Req() req: Request,
  ): Promise<boolean> {
    try {
      const { jtoken } = headers;
      await this.userService.signOut(jtoken);

      req.session.destroy((e) => console.log(e));
      res.status(200).json(true);
      return;
    } catch (err) {
      throw err;
    }
  }
  // update user ********************************
  @ApiOperation({ summary: 'update user' })
  @ApiOkResponse({
    description: 'update user',
    type: UserDto,
  })
  @ApiNotFoundResponse({ description: 'user not found' })
  @ApiInternalServerErrorResponse({
    description: 'update faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Put('')
  async updateUser(
    @Res() res: Response,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<UserDto> {
    try {
      const user = await this.userService.updateUser(
        updateUserDto,
        res.locals.user.id,
      );
      res.status(200).json(user);
      return;
    } catch (err) {
      throw err;
    }
  }
  // get user inf ********************************
  @ApiOperation({ summary: 'get user inf' })
  @ApiOkResponse({
    description: ' user inf',
    type: UserDto,
  })
  @ApiNotFoundResponse({ description: 'user not found' })
  @ApiInternalServerErrorResponse({
    description: ' get inf faild',
  })
  @ApiHeader({
    name: 'jtoken',
    description: 'this token recieved in login',
  })
  @HttpCode(HttpStatus.OK)
  @Get('')
  async getUserInf(@Res() res: Response): Promise<UserDto> {
    try {
      const user = await this.userService.getUserInf(res.locals.user.id);
      res.status(200).json(user);
      return;
    } catch (err) {
      throw err;
    }
  }
}

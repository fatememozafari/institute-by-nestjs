import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { ValidUserMiddleware } from '../../common/middlewares/validateUser.middleware';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserDataAccess } from '../../dataAccess/user.dataAccess';
import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helper';
import { ConvertDate } from '../../common/helpers/convertDate.helper';
import { ContactModule } from './contactUs/contact.module';
import { OfflineCourseModule } from './offlineCourse/offlineCourse.module';
import { CourseModule } from './course/course.module';
import { EnrollModule } from './enroll/enroll.module';

@Module({
  imports: [ContactModule, OfflineCourseModule, CourseModule, EnrollModule],
  controllers: [UserController],
  providers: [UserService, UserDataAccess, Jwt, Tools, ConvertDate],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ValidUserMiddleware).forRoutes(
      'contactUs',
      'course',
      'enroll',
      'offlineCourse',
      {
        path: 'user',
        method: RequestMethod.GET,
      },
      {
        path: 'user',
        method: RequestMethod.PUT,
      },
    );
  }
}

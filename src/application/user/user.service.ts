import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import md5 = require('md5');
import {
  CreateUserDto,
  LoginUserDto,
  UpdateUserDto,
  UserDto,
  userObj,
} from '../../DTO/user.dto';
import { Jwt } from '../../common/helpers/jwt.helper';
import { Tools } from '../../common/helpers/tools.helper';
import { UserDataAccess } from '../../dataAccess/user.dataAccess';
import userGender from '../../common/eNums/userGender.enum';

@Injectable()
export class UserService {
  constructor(
    private readonly userDataAcceess: UserDataAccess,
    private readonly jwt: Jwt,
    private readonly tools: Tools,
  ) {}

  // register user ************************************
  async register(userCreateDto: CreateUserDto): Promise<UserDto> {
    const {
      firstName,
      lastName,
      mobile,
      email,
      password,
      passwordConfirmation,
      avatar,
      gender,
      address,
    } = userCreateDto;
    const userMobile = await this.userDataAcceess.findUserByMobile(mobile);
    if (userMobile) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'duplicate mobile number',
        },
        HttpStatus.CONFLICT,
      );
    }
    const userEmail = await this.userDataAcceess.findUserByEmail(email);
    if (userEmail) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'duplicate email',
        },
        HttpStatus.CONFLICT,
      );
    }
    if (password !== passwordConfirmation) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_ACCEPTABLE,
          error: 'password confirm not match',
        },
        HttpStatus.NOT_ACCEPTABLE,
      );
    }
    const genderCheck = Object.keys(userGender).find(
      (key) => userGender[key].code === gender,
    );
    await this.userDataAcceess.createUserRequest(
      firstName,
      lastName,
      mobile,
      email,
      md5(password),
      md5(passwordConfirmation),
      avatar,
      genderCheck ? gender : 1,
      address,
    );

    return this.login({ mobile, password });
  }
  // updateUser **************************************
  async updateUser(updateUserDto: UpdateUserDto, id: number): Promise<UserDto> {
    const {
      firstName,
      lastName,
      email,
      password,
      passwordConfirmation,
      avatar,
      address,
    } = updateUserDto;
    let gender = updateUserDto.gender;
    const genderCheck = Object.keys(userGender).find(
      (key) => userGender[key].code === gender,
    );
    const user = await this.userDataAcceess.findUserById(id);
    const userEmail = await this.userDataAcceess.findUserByEmail(email);
    if (userEmail && email !== user.email) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          error: 'duplicate email',
        },
        HttpStatus.CONFLICT,
      );
    }
    gender = genderCheck ? gender : 1;
    await this.userDataAcceess.updateUser(
      id,
      firstName,
      lastName,
      email,
      md5(password),
      md5(passwordConfirmation),
      avatar,
      gender,
      address,
    );
    const updatedUser = await this.userDataAcceess.findUserById(id);
    return userObj(updatedUser, updatedUser.jwtToken);
  }

  // login ***************************************************
  async login(loginUserDto: LoginUserDto): Promise<UserDto> {
    const { mobile, password } = loginUserDto;
    const user = await this.userDataAcceess.findUserByMobile(mobile);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Login failed',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    if (user.password !== md5(password)) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Login failed',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    const tokenValues = {
      userId: user.id,
    };
    const jwtToken = this.jwt.signer(tokenValues, 86400 * 2);
    await this.userDataAcceess.updataJwtToken(user.id, jwtToken);
    return userObj(user, jwtToken);
  }
  // logOut ********************
  async signOut(jwtToken) {
    const user = await this.userDataAcceess.findUserToken(jwtToken);
    if (user) {
      await this.userDataAcceess.signOut(user.id);
    }

    return;
  }
  // get User Inf ********************
  async getUserInf(userId): Promise<UserDto> {
    const user = await this.userDataAcceess.findUserById(userId);
    return userObj(user, user.jwtToken);
  }
  // // search uaser ********************************
  // async searchUsers(str: string): Promise<UserDto[]> {
  //   const users = await this.userDataAcceess.searchUsers(str, str, str);
  //   return users.map((user) => userObj(user));
  // }
}

const courseStatus = {
  active: {
    code: 1,
    text: 'active',
  },
  inActive: {
    code: 2,
    text: 'inActive',
  },
};
export default courseStatus;

const courseTypes = {
  general: {
    code: 1,
    text: 'general',
  },
  specialized: {
    code: 2,
    text: 'specialized',
  },
  semiSpecialized: {
    code: 3,
    text: 'semiSpecialized',
  },
};
export default courseTypes;

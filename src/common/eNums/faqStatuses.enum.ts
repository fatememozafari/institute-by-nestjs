const faqStatuses = {
  newQuestion: {
    code: 1,
    text: 'newQuestion',
  },
  answered: {
    code: 2,
    text: 'answered',
  },
};
export default faqStatuses;

const userGender = {
  male: {
    code: 1,
    text: 'male',
  },
  female: {
    code: 2,
    text: 'female',
  },
  other: {
    code: 3,
    text: 'other',
  },
};
export default userGender;
import {
  Injectable,
  NestMiddleware,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { AdminDataAccess } from 'src/dataAccess/admin.dataAccess';
import { adminObj } from 'src/DTO/admin.dto';
import { Jwt } from '../helpers/jwt.helper';

@Injectable()
export class ValidAdminMiddleware implements NestMiddleware {
  constructor(
    private readonly adminDataAccess: AdminDataAccess,
    private readonly jwt: Jwt,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.session.admin) {
      res.locals.admin = req.session.admin;
      next();
    } else if (req.header('jtoken') && req.header('jtoken').length > 0) {
      const token = req.header('jtoken');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        next(
          new HttpException(
            {
              status: HttpStatus.FORBIDDEN,
              error: 'INVALID_TOKEN',
            },
            HttpStatus.FORBIDDEN,
          ),
        );
        return;
      }
      const activeToken = await this.adminDataAccess.findAdminToken(token);
      if (!activeToken) {
        next(
          new HttpException(
            {
              status: HttpStatus.FORBIDDEN,
              error: 'ACCESS_DENIED',
            },
            HttpStatus.FORBIDDEN,
          ),
        );
        return;
      } else {
        const admin = adminObj(activeToken, token);
        req.session.admin = admin;
        res.locals.admin = admin;
        next();
      }
    } else {
      next(
        new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'ACCESS_DENIED',
          },
          HttpStatus.FORBIDDEN,
        ),
      );
      return;
    }
  }
}

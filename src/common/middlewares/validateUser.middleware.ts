import {
  Injectable,
  NestMiddleware,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { UserDataAccess } from 'src/dataAccess/user.dataAccess';
import { userObj } from 'src/DTO/user.dto';
import { Jwt } from '../helpers/jwt.helper';

@Injectable()
export class ValidUserMiddleware implements NestMiddleware {
  constructor(
    private readonly userDataAccess: UserDataAccess,
    private readonly jwt: Jwt,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    // console.log('sssssssssssssssss');
    if (req.session.user) {
      res.locals.user = req.session.user;
      next();
    } else if (req.header('jtoken') && req.header('jtoken').length > 0) {
      const token = req.header('jtoken');
      const tokenValues = this.jwt.verifier(token);
      if (!tokenValues) {
        next(
          new HttpException(
            {
              status: HttpStatus.FORBIDDEN,
              error: 'INVALID_TOKEN',
            },
            HttpStatus.FORBIDDEN,
          ),
        );
        return;
      }
      const activeToken = await this.userDataAccess.findUserToken(token);
      if (!activeToken) {
        next(
          new HttpException(
            {
              status: HttpStatus.FORBIDDEN,
              error: 'ACCESS_DENIED',
            },
            HttpStatus.FORBIDDEN,
          ),
        );
        return;
      } else {
        const user = userObj(activeToken, token);
        req.session.user = user;
        res.locals.user = user;
        next();
      }
    } else {
      next(
        new HttpException(
          {
            status: HttpStatus.FORBIDDEN,
            error: 'ACCESS_DENIED',
          },
          HttpStatus.FORBIDDEN,
        ),
      );
      return;
    }
  }
}

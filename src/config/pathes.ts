import * as path from 'path';

const pathes = {
  staticFiles: './public',
  viewFiles: './views',
  userQrCodeFiles: './public/users/',
  uploadedFiles: '/files/',
  tinymceUpload: '/tinymceUpload/',
  siteUrl: 'http://localhost:3000',
  appRoot: path.resolve(),
};

export default pathes;

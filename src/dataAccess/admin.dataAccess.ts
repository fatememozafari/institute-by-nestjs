import * as Models from '../models/index';

export class AdminDataAccess {
  async loginAdmin(userName): Promise<Models.Admin> {
    const admin = await Models.Admin.findOne({
      where: {
        userName: userName,
      },
    });
    return admin;
  }
  async signOut(adminId): Promise<boolean> {
    await Models.Admin.update(
      { jwtToken: '' },
      {
        where: {
          id: adminId,
        },
      },
    );
    return true;
  }
  async updataJwtToken(adminId, jwtToken): Promise<boolean> {
    await Models.Admin.update(
      { jwtToken },
      {
        where: {
          id: adminId,
        },
      },
    );
    return true;
  }
  async findAdminByuserName(userName) {
    const admin = await Models.Admin.findOne({
      where: {
        userName: userName,
      },
    });
    return admin;
  }
  async findAdminToken(jwtToken) {
    const admin = await Models.Admin.findOne({
      where: {
        jwtToken: jwtToken,
      },
    });
    return admin;
  }
}

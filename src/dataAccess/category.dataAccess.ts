import * as Models from '../models/index';

export class CategoryDataAccess {
  async adminCreateCategory(name: string): Promise<Models.Category> {
    const category = await Models.Category.create({
      name,
    });
    return category;
  }
  async findAllCategory(): Promise<Models.Category[]> {
    const categories = await Models.Category.findAll();
    return categories;
  }
  async findCategoryById(categoryId): Promise<Models.Category> {
    const category = await Models.Category.findByPk(categoryId);
    return category;
  }
}

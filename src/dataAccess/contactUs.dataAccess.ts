import * as Models from '../models/index';

export class ContactDataAccess {
  async createContact(
    name,
    title,
    message,
    file,
    categoryId,
    userId,
  ): Promise<Models.Contact> {
    const newContact = await Models.Contact.create({
      name,
      title,
      message,
      file,
      categoryId,
      status: 1,
      userId,
    });
    return newContact;
  }
  async AdminFindAllContact(): Promise<Models.Contact[]> {
    const contacts = await Models.Contact.findAll({
      include: [Models.User, Models.Category],
    });
    return contacts;
  }
  async AdminShowContactInfo(contactId): Promise<Models.Contact> {
    const contact = await Models.Contact.findOne({
      where: {
        id: contactId,
      },
      include: [Models.User, Models.Category],
    });
    return contact;
  }

  async AdminAnswerToContact(id, answer) {
    await Models.Contact.update(
      {
        answer,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
    return;
  }

}

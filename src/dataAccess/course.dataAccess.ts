import * as Models from '../models/index';
import courseStatus from 'src/common/eNums/courseStatus.enum';

export class CourseDataAccess {
  async adminCreateNewCourse(
    title,
    slug,
    courseType,
    startDate,
    endDate,
    duration,
    // rate,
    teacher,
    categoryId,
    // userId,
  ): Promise<Models.Course> {
    const newCourse = await Models.Course.create({
      title,
      slug,
      courseType,
      status: 1,
      startDate,
      endDate,
      duration,
      rate: 0,
      teacher,
      categoryId,
      // userId,
    });
    return newCourse;
  }
  async findAllCourse(): Promise<Models.Course[]> {
    const courses = await Models.Course.findAll({
      include: [Models.Category],
    });
    return courses;
  }
  async findAllValidCourse(): Promise<Models.Course[]> {
    const today = new Date();
    const courses = await Models.Course.findAll({
      where: {
        status: courseStatus.active.code,
        startDate: {
          $lte: today,
        },
        endDate: {
          $gte: today,
        },
      },
      include: [Models.Category],
    });
    return courses;
  }
  async findAllCourseByCategory(categoryId): Promise<Models.Course[]> {
    const courses = await Models.Course.findAll({
      where: {
        categoryId: categoryId,
      },
      include: [Models.Category],
    });
    return courses;
  }

  async findAllCourseByCourseType(courseType): Promise<Models.Course[]> {
    const courses = await Models.Course.findAll({
      where: {
        courseType: courseType,
      },
      include: [Models.Category],
    });
    return courses;
  }

  async findOneCourse(courseId): Promise<Models.Course> {
    const course = await Models.Course.findOne({
      where: {
        id: courseId,
        status: courseStatus.active.code,
      },
      include: [Models.Category],
    });
    return course;
  }

  async findOneCourseByTitle(title): Promise<Models.Course> {
    const course = await Models.Course.findOne({
      where: {
        title: { $like: `%${title}%` },
      },
      include: [Models.Category],
    });
    return course;
  }

  async adminUpdateCourse(
    id,
    title,
    slug,
    status,
    startDate,
    endDate,
    duration,
  ) {
    await Models.Course.update(
      {
        title,
        slug,
        status,
        startDate,
        endDate,
        duration,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
    return;
  }

  async showCourseInfo(courseId): Promise<Models.Course> {
    const course = await Models.Course.findOne({
      where: {
        id: courseId,
      },
      include: [Models.Category],
    });
    return course;
  }

  //************************************** filter ************************************** */

  async filterCourse(
    title,
    courseType,
    categoryId,
    startDate,
    endDate,
    // pageSize,
    // page,
  ): Promise<Models.Course[]> {
    const where: any = {};
    where.status = courseStatus.active.code;
    if (title && title.length > 2) {
      where.title = { $like: `%${title}%` };
    }
    if (categoryId && parseInt(categoryId) > 0) {
      where.categoryId = categoryId;
    }
    if (startDate) {
      if (new Date(startDate)) {
        where.startDate = {
          $gte: startDate,
        };
      }
    }
    if (endDate) {
      if (new Date(endDate)) {
        where.endDate = {
          $lte: endDate,
        };
      }
    }
    if (courseType && parseInt(courseType) > 0) {
      where.courseType = courseType;
    }
    const courses = await Models.Course.findAll({
      where,
      // limit: pageSize || 10,
      // offset: page * pageSize || 0,
      order: [['id', 'DESC']],
      include: [Models.Category],
      // logging: console.log,
    });
    return courses;
  }
}

import { Model } from 'sequelize';
import * as Models from '../models/index';

export class EnrollDataAccess {
  async CreateEnroll(courseId, userId): Promise<Models.Enroll> {
    const newEnroll = await Models.Enroll.create({
      courseId,
      userId,
    });
    return newEnroll;
  }
  async findAllEnroll(userId): Promise<Models.Enroll[]> {
    const enrolls = await Models.Enroll.findAll({
      where: {
        userId: userId,
      },
      include: [
        Models.User,
        {
          model: Models.Course,
          include: [Models.Category],
        },
      ],
    });
    return enrolls;
  }
  async AdminFindAllEnroll(): Promise<Models.Enroll[]> {
    const enrolls = await Models.Enroll.findAll({
      include: [
        Models.User,
        {
          model: Models.Course,
          include: [Models.Category],
        },
      ],
    });
    return enrolls;
  }
  async findOneCourse(courseId, userId): Promise<Models.Enroll> {
    const enroll = await Models.Enroll.findOne({
      where: {
        courseId,
        userId,
      },
      include: [
        Models.User,
        {
          model: Models.Course,
          include: [Models.Category],
        },
      ],
    });
    return enroll;
  }
  async adminUpdateScore(score, courseId, userId) {
    await Models.Enroll.update(
      {
        score,
        updatedAt: new Date(),
      },
      {
        where: {
          courseId: courseId,
          userId: userId,
        },
      },
    );
    return;
  }
  async DeleteEnroll(enrollId, userId): Promise<Models.Enroll> {
    await Models.Enroll.destroy({
      where: {
        id: enrollId,
        userId,
      },
    });
    return;
  }
  async ShowEnrollInfo(enrollId): Promise<Models.Enroll> {
    const enroll = await Models.Enroll.findOne({
      where: {
        id: enrollId,
      },
      include: [
        Models.User,
        {
          model: Models.Course,
          include: [Models.Category],
        },
      ],
    });
    return enroll;
  }
}

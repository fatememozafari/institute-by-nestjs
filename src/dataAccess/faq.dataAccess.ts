import * as Models from '../models/index';

export class FaqDataAccess {
  async AdminCreateFaq(
    question,
    answer,
    file,
    categoryId,
  ): Promise<Models.Faq> {
    const newFaq = await Models.Faq.create({
      question,
      answer,
      file,
      categoryId,
    });
    return newFaq;
  }
  async findAllFaq(): Promise<Models.Faq[]> {
    const Faqs = await Models.Faq.findAll({
      include: [Models.Category],
    });
    return Faqs;
  }
  async showFaqInfo(faqId): Promise<Models.Faq> {
    const faq = await Models.Faq.findOne({
      where: {
        id: faqId,
      },
      include: [Models.Category],
    });
    return faq;
  }

  async adminDeleteFaq(faqId): Promise<Models.Faq> {
    await Models.Faq.destroy({
      where: {
        id: faqId,
      },
    });
    return;
  }
}

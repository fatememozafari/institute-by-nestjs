import * as Models from '../models/index';
import courseStatus from 'src/common/eNums/courseStatus.enum';

export class OfflineCourseDataAccess {
  async findAllOfflineCourse(): Promise<Models.OfflineCourse[]> {
    const courses = await Models.OfflineCourse.findAll({
      include: [Models.Category],
    });
    return courses;
  }
  async findAllOfflineCourseByCategory(
    categoryId,
  ): Promise<Models.OfflineCourse[]> {
    const courses = await Models.OfflineCourse.findAll({
      where: {
        categoryId: categoryId,
      },
      include: [Models.Category],
    });
    return courses;
  }
  async findAllOfflineCourseByCourseType(
    courseType,
  ): Promise<Models.OfflineCourse[]> {
    const courses = await Models.OfflineCourse.findAll({
      where: {
        courseType: courseType,
      },
      include: [Models.Category],
    });
    return courses;
  }
  async findOneOfflineCourse(courseId): Promise<Models.OfflineCourse> {
    const course = await Models.OfflineCourse.findOne({
      where: {
        id: courseId,
        status: courseStatus.active.code,
      },
      include: [Models.Category],
    });
    return course;
  }
  async findOneOfflineCourseByTitle(title): Promise<Models.OfflineCourse> {
    const course = await Models.OfflineCourse.findOne({
      where: {
        title: { $like: `%${title}%` },
      },
      include: [Models.Category],
    });
    return course;
  }
  async adminCreateOfflineCourse(
    title,
    slug,
    courseType,
    // status,
    duration,
    // rate,
    teacher,
    categoryId,
    description,
    file,
    // adminId,
  ): Promise<Models.OfflineCourse> {
    const newCourse = await Models.OfflineCourse.create({
      title,
      slug,
      courseType,
      status: 1,
      duration,
      rate: 0,
      teacher,
      categoryId,
      description,
      file,
      // adminId,
    });
    return newCourse;
  }
  async adminUpdateOfflineCourse(
    id,
    title,
    slug,
    status,
    duration,
    description,
    file,
  ) {
    await Models.OfflineCourse.update(
      {
        title,
        slug,
        status,
        duration,
        description,
        file,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
    return;
  }
  async showOfflineCourseInfo(courseId): Promise<Models.OfflineCourse> {
    const course = await Models.OfflineCourse.findOne({
      where: {
        id: courseId,
      },
      include: [Models.Category],
    });
    return course;
  }

  //************************************** filter ************************************** */

  async filterOfflineCourse(
    title,
    courseType,
    categoryId,
  ): Promise<Models.OfflineCourse[]> {
    const where: any = {};
    where.status = courseStatus.active.code;
    if (title && title.length > 2) {
      where.title = { $like: `%${title}%` };
    }
    if (categoryId && categoryId > 0) {
      where.categoryId = categoryId;
    }
    if (courseType && parseInt(courseType) > 0) {
      where.courseType = courseType;
    }
    const courses = await Models.OfflineCourse.findAll({
      where,
      order: [['id', 'DESC']],
      include: [Models.Category],
      // logging: console.log,
    });
    return courses;
  }
}

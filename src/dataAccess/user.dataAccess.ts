import userStatus from '../common/eNums/userStatus.enum';
import * as Models from '../models/index';
import { Identifier } from 'sequelize/types';

export class UserDataAccess {
  async checkDuplicate(mobile) {
    const user = await Models.User.findOne({
      where: {
        mobile: mobile,
      },
    });
    return user;
  }
  async createUserRequest(
    firstName,
    lastName,
    mobile,
    email,
    password,
    passwordConfirmation,
    avatar,
    gender,
    address,
  ): Promise<Models.User> {
    const user = await Models.User.create({
      firstName,
      lastName,
      mobile,
      email,
      password,
      passwordConfirmation,
      avatar,
      gender,
      address,
      rate: 0,
      status: 1,
    });
    return user;
  }
  async findUserById(id: Identifier): Promise<Models.User> {
    const user = await Models.User.findOne({
      where: {
        id: id,
        status: userStatus.active.code,
      },
    });
    return user;
  }
  async findUserToken(jwtToken) {
    const user = await Models.User.findOne({
      where: {
        jwtToken: jwtToken,
        status: userStatus.active.code,
      },
    });
    return user;
  }
  async findUserByEmail(email) {
    const user = await Models.User.findOne({
      where: {
        email: email,
      },
    });
    return user;
  }
  async findUserByMobile(mobile) {
    const user = await Models.User.findOne({
      where: {
        mobile: mobile,
        status: userStatus.active.code,
      },
    });
    return user;
  }
  async updateUser(
    id,
    firstName,
    lastName,
    email,
    password,
    passwordConfirmation,
    avatar,
    gender,
    address,
  ) {
    await Models.User.update(
      {
        firstName,
        lastName,
        email,
        password,
        passwordConfirmation,
        avatar,
        gender,
        address,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
    return;
  }
  async changeStatusUser(id, status) {
    await Models.User.update(
      {
        status,
        updatedAt: new Date(),
      },
      {
        where: {
          id,
        },
      },
    );
    return;
  }
  async loginUserRequest(mobile): Promise<Models.User> {
    const user = await Models.User.findOne({
      where: {
        mobile: mobile,
      },
    });
    return user;
  }
  async signOut(userId): Promise<boolean> {
    await Models.User.update(
      { jwtToken: '' },
      {
        where: {
          id: userId,
        },
      },
    );
    return true;
  }
  async updataJwtToken(userId, jwtToken): Promise<boolean> {
    await Models.User.update(
      { jwtToken },
      {
        where: {
          id: userId,
        },
      },
    );
    return true;
  }
}

import { Column, Table, Model, HasMany } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { OfflineCourse } from './offlineCourse.model';

@Table({
  tableName: 'admins',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Admin extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  userName: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  password: string;

  @Column({
    type: Sequelize.TEXT,
    allowNull: true,
  })
  jwtToken: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @HasMany(() => OfflineCourse, { foreignKey: 'adminId' })
  OfflineCourse: OfflineCourse[];
}

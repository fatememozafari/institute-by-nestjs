import { Column, Table, Model, HasMany, BelongsTo } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Category } from './category.model';
import { User } from './user.model';

@Table({
  tableName: 'contacts',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Contact extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  name: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  title: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  message: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  file: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  answer: string;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    references: { model: 'categories', key: 'id' },
  })
  categoryId: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => Category, { foreignKey: 'categoryId' })
  Category: Category;
}

import { Column, Table, Model, HasMany, BelongsTo } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Category } from './category.model';
import { User } from './user.model';
import { Course } from './course.model';

@Table({
  tableName: 'enrolls',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Enroll extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    references: { model: 'users', key: 'id' },
  })
  userId: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    references: { model: 'courses', key: 'id' },
  })
  courseId: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: true,
  })
  score: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => User, { foreignKey: 'userId' })
  User: User;

  @BelongsTo(() => Course, { foreignKey: 'courseId' })
  Course: Course;
}

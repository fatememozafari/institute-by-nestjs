import { Column, Table, Model, HasMany, BelongsTo } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Category } from './category.model';
import { User } from './user.model';

@Table({
  tableName: 'faqs',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class Faq extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  question: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  answer: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  file: string;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    references: { model: 'categories', key: 'id' },
  })
  categoryId: number;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  @BelongsTo(() => Category, { foreignKey: 'categoryId' })
  Category: Category;
}

import { Column, Table, Model, HasMany, BelongsTo } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Category } from './category.model';
import { User } from './user.model';
import { Admin } from './admin.model';

@Table({
  tableName: 'offlineCourses',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class OfflineCourse extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  title: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  slug: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  courseType: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  teacher: string;

  // @Column({
  //   type: Sequelize.INTEGER,
  //   allowNull: true,
  //   references: { model: 'admins', key: 'id' },
  // })
  // adminId: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
    references: { model: 'categories', key: 'id' },
  })
  categoryId: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  file: string;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  duration: number;

  @Column({
    allowNull: true,
    type: Sequelize.INTEGER,
  })
  rate: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
  })
  status: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  description: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  // @BelongsTo(() => User, { foreignKey: 'userId' })
  // User: User;

  // @BelongsTo(() => Admin, { foreignKey: 'adminId' })
  // Admin: Admin;

  @BelongsTo(() => Category, { foreignKey: 'categoryId' })
  Category: Category;
}

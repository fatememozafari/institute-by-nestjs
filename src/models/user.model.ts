import { Column, Table, Model, HasMany } from 'sequelize-typescript';
import Sequelize from 'sequelize';
import { Course } from './course.model';

@Table({
  tableName: 'users',
  paranoid: true,
  deletedAt: 'deletedAt',
})
export class User extends Model {
  @Column({
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  })
  id: number;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  firstName: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  lastName: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  })
  mobile: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  email: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  password: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: false,
  })
  passwordConfirmation: string;

  @Column({
    type: Sequelize.STRING,
    allowNull: true,
  })
  avatar: string;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: true,
  })
  gender: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: true,
  })
  rate: number;

  @Column({
    type: Sequelize.INTEGER,
    allowNull: false,
  })
  status: number;

  @Column({
    type: Sequelize.TEXT,
    allowNull: true,
  })
  jwtToken: string;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  createdAt: Date;

  @Column({
    defaultValue: new Date(),
    allowNull: false,
    type: Sequelize.DATE,
  })
  updatedAt: Date;

  @Column({
    allowNull: true,
    type: Sequelize.DATE,
  })
  deletedAt: Date;

  //@HasMany(() => Course, { foreignKey: 'userId' })
  //Courses: Course[];
}
